## To launch a webserver
```
python -m SimpleHTTPServer 8000
```

## About the model
Model implemented from Mazza et al, NAR, 2012.
